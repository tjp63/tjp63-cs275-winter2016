package com.example.tyler.tictactoe;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.example.tyler.tictactoe.R.menu.menu_activity;

public class MainActivity extends ActionBarActivity {

    public String turn = "x"; //X will go first.
    public int [] board = new int[9]; //board array to know which positions have been selected
    public int count = 0; //number of turns.

    //This will run once the program has been launched on the phone. It will reset the Board
    //of any previous values and then set the view to be displayed to the user.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resetBoard();
        setContentView(R.layout.activity_main);
    }

    //This will add the options menu to the top right section of the screen.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(menu_activity, menu);
        return true;
    }

    //If an item is selected from the options menu, then this method will execute
    //the MenuItem that is passed into the method is the item that was
    //selected by the user.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *
     *  This method will reset all the values on the board. This happens when
     *  the onCreate method is executed or when the "New Game" Button is
     *  selected.
     *
     */
    public void resetBoard(){
        for(int i = 0; i<9; i++) {
            board[i] = 10;
        }
    }


    /**
     *
     * This method handles the action of one of the 9 buttons that represent
     * the game board is clicked. It will pull down the button that was clicked,
     * get the id of the button, and then use that ID to determine which button
     * on the grid was clicked.
     *
     * Then depending on the turn, it will set the Text of the button to display
     * either an X or an O. Once the text has been displlayed, the value in the
     * board array is updated and then turn is switched to the opposite shape.
     * @param v The button that was clicked.
     */
    public void clickButton(View v){
        count++;
        Button b = (Button) v;
        String strId = ((String)b.getTag());
        int tag = Integer.parseInt(strId);
        if(turn.equals("x")) {
            b.setText(R.string.X);
            board[tag]=1;
            turn = "o";
        } else {
            b.setText(R.string.O);
            board[tag]=0;
            turn="x";
        }
        b.setClickable(false);
        checkWin(tag);
    }

    /**
     * This method handles the validation of if the game is finished. it will go through
     * and determine the score of each player, and then check and see if that score is enough
     * to win the game. By score I mean it will count the values in the board integer array.
     *
     *
     * @param tag
     */
    public void checkWin(Integer tag) {
        int mod = tag%3;
        int vertSum = 30;
        int horSum = 30;
        int diagSumLeft = 30;
        int diagSumRight = 30;

        //Switch statement to determine where the most recent button was
        //pressed. If 0 it is in the first row, if 1 it is in the second row,
        //and if 2 it is in the final row.
        switch(mod) {
            case 0:
                //Adding vertical sum, horizontal sum, and the cross sum in each direction.
                vertSum = board[0]+board[3]+board[6];
                horSum = board[tag]+board[tag+1]+board[tag+2];
                if((tag==0)||(tag==6)){
                    diagSumRight = board[0]+board[4]+board[8];
                    diagSumLeft = board[6]+board[4]+board[2];
                }
                break;
            case 1:
                vertSum = board[1]+board[4]+board[7];
                horSum = board[tag-1]+board[tag]+board[tag+1];
                if((tag==4)) {
                    diagSumRight = board[0]+board[4]+board[8];
                    diagSumLeft = board[6]+board[4]+board[2];
                }
                break;
            case 2:
                vertSum = board[2]+board[5]+board[8];
                horSum = board[tag-2]+board[tag-1]+board[tag];
                if((tag==2)||(tag==8)){
                    diagSumRight = board[0]+board[4]+board[8];
                    diagSumLeft = board[6]+board[4]+board[2];
                }
        }

        //If the sum of any direction equals 0, then the O player has won the game.
        if((vertSum==0)||(horSum==0)||(diagSumLeft==0)||(diagSumRight==0)) {
            TextView results = (TextView) findViewById(R.id.results);
            results.setText(R.string.oWin);
            results.setTextColor(Color.parseColor("#BC0903"));
            results.setBackgroundColor(Color.parseColor("#BCBABA"));
            stopGame();
        }
        //if statement that will handle if the X player wins the game.
        else if((vertSum==3)||(horSum==3)||(diagSumLeft==3)||(diagSumRight==3)) {
            TextView results = (TextView) findViewById(R.id.results);
            results.setText(R.string.xWin);
            results.setTextColor(Color.parseColor("#BC0903"));
            results.setBackgroundColor(Color.parseColor("#BCBABA"));
            stopGame();
        }
        //If statement that will be hit if all 9 spaces have filled up and there is
        //no winner declared.
        else if(count==9){
            TextView results = (TextView) findViewById(R.id.results);
            results.setText(R.string.tie);
            results.setTextColor(Color.parseColor("#BC0903"));
            results.setBackgroundColor(Color.parseColor("#BCBABA"));
            stopGame();
        }
    }

    //This makes the game end, and it makes all the buttons unable to be clicked.
    //This is only called if there is a Tie or if one of the players wins i
    public void stopGame() {
        int id = R.id.button1;
        for(int i=0; i<9; i++){
            Button b = (Button) findViewById(id+i);
            b.setClickable(false);
        }
    }

    public void OnNewGameButtonClick(View v) {
        int id = R.id.button1;
        for(int i=0; i<9; i++){
            Button b = (Button) findViewById(id+i);
            b.setText(" ");
            b.setClickable(true);
        }
        turn="x";
        resetBoard();
        count=0;
        TextView results = (TextView) findViewById(R.id.results);
        results.setText("Play!");
        results.setTextColor(Color.parseColor("#ffffff"));
        results.setBackgroundColor(Color.parseColor("#A1E3E2"));
    }
}
