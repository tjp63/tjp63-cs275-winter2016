Tyler Pingree
CS 275 Web & Mobile Application Development
Lab 3
Email: tjp63@drexel.edu tyler.pingree@gmail.com

For this lab, which is the first Android application lab. We were tasked with creating an
android application that would handle the functionality behind playing the game Tic Tac Toe.
Using the GUI interface, a 3x3 grid will be created that will hold all the tiles for the game.
There also needs to be a status of the game i.e.(X wins, O wins, Tie Game) as well as a "New Game Button"

Everytime a player takes a turn, which ever button they select will change to blank text
and will display either an X or an O. After the button has been clicked, a method is called
that will handle checking the board to see if a player has won the game or if there is a tie.
This is done within the checkBoard() method that is gone over in the screencast.

If there is no winner and there are still buttons to be filled in, the player is able to select
another tile on the tic tac toe board. Once a button is clicked, it is made unclickable so there 
is no way for a player to change his pick. In addition, once the board is filled and there is a winner
or a tie game, the stopGame() method is called. Within that method, it clears all the values that
are on the board, it clears all the text that is being displayed to the user as well.

Testing:
In order to test this program, there are three different scenarios. X player wins, O player wins,
and there is a tie game. Each scenario was drawn out in the screen cast so you can see it be 
executed for yourself.

 