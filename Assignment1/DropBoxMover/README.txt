Tyler Pingree
CS 275 Web & Mobile Application Development
Assignment 1
Email: tjp63@drexel.edu tyler.pingree@gmail.com

For this assignment, we were asked to utilize DropBox and download files from a given DropBox location and
download them locally to a file path specified within the file __list.txt. The contents of my __list.txt file
are as follows:

A.txt C:\Users\tyler\Documents\NetBeansProjects\tjp63-cs275-winter2016\DropBoxMover\home\abc123\
B.txt C:\Users\tyler\Documents\NetBeansProjects\tjp63-cs275-winter2016\DropBoxMover\home\wmm24\

The paths that are provided are to my local file system. The flow of the program is as follows:
Initially the user is asked to navigate to a dropbox url that will provide my program with authorization
to use their dropbox and search through it for the files that I need. Once authentication is provided from
the user, the app will continue after the ENTER buttons ir pressed. Utilizing Temboo, the file name is obtained
due to the fact that the path is known and hardcoded within the code. Once the __list.txt file is obtained from the
Temboo call, it is decoded and parsed in order to know which files need to be moved, and where they need to be
moved too.

A for loop parses through the contents of __list.txt and for each line, a set of instructions is executed
by the program. The program first searches through the entire DropBox file system the user has authorized
in order to determine where the file that was given is located. Once the file is downloaded, it then is
parsed through using a writer and a BufferedOutputStream. These two work together to take the file contents
found on DropBox and write them to a local file loaction.

Once all the commands have been executed, the __list.txt file is deleted from the DropBox repository.

Testing: This involved debugging through the program to figure out while file paths were not
being parsed correctly. I also didn't add the __list.txt delete method until after all the functionality
worked properly.