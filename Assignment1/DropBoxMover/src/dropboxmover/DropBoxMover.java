
package dropboxmover;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders.SearchFilesAndFoldersInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders.SearchFilesAndFoldersResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooSession;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Scanner;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Tyler Pingree
 */
public class DropBoxMover {

    /**
     * @param args the command line arguments
     */
    
    public static String accessToken; //public static variable to work in various methods
    public static String tokenSecret; //public static variable to work in various methods
    public static String callbackId; //public static variable to work in various methods
    public static String token; //public static variable to work in various methods
    
    
    public static void main(String[] args) throws Exception{
        
        
        //Temboo App key and secret. As well as dropbox path for __list.txt
        String appKey = "2r8up2eq4twh7kk";
        String appSecret = "uezzqqy9606jbed";
        String path = "move/__list.txt";
        
        TembooSession session = new TembooSession("tylerpingree","myFirstApp","JeIu8x8pJaj8oDmDmIhn27X4Otw7p5zw");
        
        //method call to finalize authentication protocol.
        finalizeOauth(session, appKey, appSecret);
        
        //Creating the getFile object and setting inputs.
        GetFile moveFile = new GetFile(session);
        GetFileInputSet fileInput = moveFile.newInputSet();
        fileInput.set_AppSecret(appSecret);
        fileInput.set_AccessToken(accessToken);
        fileInput.set_AccessTokenSecret(tokenSecret);
        fileInput.set_AppKey(appKey);
        fileInput.set_Path(path);
        
        //Gets the file result, decodes the encoded string that is returned, and then
        //splits the file into two separate strings within an array so it can be
        //easily parsed.
        GetFileResultSet fileResult = moveFile.execute(fileInput);
        String fileContent = new String(Base64.decodeBase64(fileResult.get_Response()));
        String[] files = fileContent.split("\r\n");
        
        
        for(int i = 0; i<files.length; i++){
            //Spliting the command, and the path to send that command into its own array.
            String[] file = files[i].split(" ");
            
            SearchFilesAndFolders searchFilesAndFoldersChoreo1 = new SearchFilesAndFolders(session);
            // Get an InputSet object for the choreo
            SearchFilesAndFoldersInputSet searchFilesAndFoldersInputs1 = searchFilesAndFoldersChoreo1.newInputSet();

            // Set inputs
            searchFilesAndFoldersInputs1.set_AccessToken(accessToken);
            searchFilesAndFoldersInputs1.set_AccessTokenSecret(tokenSecret);
            searchFilesAndFoldersInputs1.set_AppKey(appKey);
            searchFilesAndFoldersInputs1.set_AppSecret(appSecret);
            searchFilesAndFoldersInputs1.set_Query(file[0]);

            //Execute the search choreo.
            SearchFilesAndFoldersResultSet searchFilesAndFoldersResults1 = searchFilesAndFoldersChoreo1.execute(searchFilesAndFoldersInputs1);

            //Getting the file path __list.txt from the JSOn response that was returned from temboo.
            JsonParser jp1 = new JsonParser();
            JsonElement root1 = jp1.parse(searchFilesAndFoldersResults1.get_Response());
            JsonArray rootArray1 = root1.getAsJsonArray();
            String filePath1 = rootArray1.get(0).getAsJsonObject().get("path").getAsString();
            
            System.out.println("Found file at: " + filePath1);

            //Get the file contents
            GetFile localFileChoreo = new GetFile(session);

            // Get an InputSet object for the choreo
            GetFileInputSet fileInputs = localFileChoreo.newInputSet();

            // Set inputs
            fileInputs.set_AccessToken(accessToken);
            fileInputs.set_AccessTokenSecret(tokenSecret);
            fileInputs.set_AppKey(appKey);
            fileInputs.set_AppSecret(appSecret);
            fileInputs.set_Path(filePath1);

            // Execute Choreo
            GetFileResultSet getResults = localFileChoreo.execute(fileInputs);
            String content = new String(Base64.decodeBase64(getResults.get_Response()));

            //Print the contents to a the local location
            Writer wr = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(file[1] + file[0]), "utf-8"));
            wr.write(content);
            
            //Closes the WriteStream and prints out to the user that the file has been copied.
            wr.close();
            System.out.println("Finished copying " + file[0] + " to " + file[1] + file[0]);
        
        }
        //Deletes the command from the __list.txt file.
        deleteFile(session, appKey, appSecret, path);
    }
    
    /**
     *  Method that handles all of the authentication that needs to be done in order to connect to
     *  Both DropBox as well as Temboo. Dropbox needs to provide this application with authorization,
     *  and then Temboo also needs authorization. 
     * 
     *  The user will be prompted to navigate to a URL, and then once they click allow, simply 
     *  pressing ENTER in the window will allow the program to continue.
     * 
     * @param session - Temboo session parameters that were created in the main method.
     * @param appKey - appKey which is a main method defined variable.
     * @param appSecret - appSecret with is a main method defined variable.
     * 
     * @throws Exception - Would be thrown if the user does not navigate to the dropbox url and allow the application 
     * to continue
     */
    public static void finalizeOauth(TembooSession session, String appKey, String appSecret) throws Exception{
       
        InitializeOAuth authChoreo = new InitializeOAuth(session);
        InitializeOAuthInputSet oAuthInputs = authChoreo.newInputSet();
        oAuthInputs.set_DropboxAppSecret(appSecret);
        oAuthInputs.set_DropboxAppKey(appKey);
        InitializeOAuthResultSet authResults = authChoreo.execute(oAuthInputs);
        
        String authURL = authResults.get_AuthorizationURL();
        
        System.out.println("Please go to: "+authURL);
        System.out.println("Press enter when complete");
        Scanner keyboard = new Scanner(System.in);
        String enter = keyboard.nextLine();
        
        
        callbackId = authResults.get_CallbackID();
        token = authResults.get_OAuthTokenSecret();
        
        FinalizeOAuth finalizeChoreo = new FinalizeOAuth(session);
        FinalizeOAuthInputSet finalInputs = finalizeChoreo.newInputSet();
        
        finalInputs.set_CallbackID(callbackId);
        finalInputs.set_DropboxAppSecret(appSecret);
        finalInputs.set_OAuthTokenSecret(token);
        finalInputs.set_DropboxAppKey(appKey);
        
        FinalizeOAuthResultSet finalResults = finalizeChoreo.execute(finalInputs);
        accessToken = finalResults.get_AccessToken();
        tokenSecret = finalResults.get_AccessTokenSecret();
    }
    
    /**
     * 
     *  This method deletes the __list.txt file from the DropBox directory at the
     *  very end of the program. This is needed to ensure that there are not any
     *  chance that the files be rewritten from the dropbox and overwritten in the new
     *  file's place.
     * 
     * @param session - Temboo session parameters that were created in the main method.
     * @param appKey - appKey which is a main method defined variable.
     * @param appSecret - appSecret with is a main method defined variable.
     * 
     * @throws Exception - Would be thrown if the user does not navigate to the dropbox url and allow the application 
     * to continue
     */
    public static void deleteFile(TembooSession session, String appKey, String appSecret, String path) throws Exception{
        DeleteFileOrFolder deleteChoreo = new DeleteFileOrFolder(session);
        DeleteFileOrFolderInputSet deleteInputs = deleteChoreo.newInputSet();
        
        deleteInputs.set_AccessToken(accessToken);
        deleteInputs.set_AccessTokenSecret(tokenSecret);
        deleteInputs.set_AppKey(appKey);
        deleteInputs.set_AppSecret(appSecret);
        deleteInputs.set_Path(path);
        
        DeleteFileOrFolderResultSet deleteResults = deleteChoreo.execute(deleteInputs);
        
        System.out.println("The files were moved.");
    }
    
    
}
