/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calendarviewer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.core.TembooSession;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 *
 * @author tyler
 */
public class CalendarViewerTemboo {
    
    public static void main(String args[]) throws Exception{
        
        //All information needed for post requests.
        String redirectUri = "urn:ietf:wg:oauth:2.0:oob";
        String clientId = "584263795773-s375gv3pp39veumnjou3h67ironkj3u8.apps.googleusercontent.com";
        String clientSecret = "GRgAR4ai7sf4gjgAXvAHCCzd";
        String appName = "myFirstApp";
        String appID = "JeIu8x8pJaj8oDmDmIhn27X4Otw7p5zw";
        String accountName = "tylerpingree";
        
        //OAuth url
        String oauthUrl = "https://accounts.google.com/o/oauth2/auth?"
				+ "access_type=offline"
				+ "&client_id=" + clientId
				+ "&scope=https://www.googleapis.com/auth/calendar"
				+ "&response_type=code"
				+ "&redirect_uri=" + redirectUri 
				+ "&state=/profile"
				+ "&approval_prompt=force";
        
        //Getting the code
        System.out.println("Go to this link and login to provide access to your calendar " + oauthUrl);
        Scanner keyboard = new Scanner(System.in);
        String code = keyboard.nextLine();
        
        //Setting auth parameters and executing the post request.
        String authURL = "https://accounts.google.com/o/oauth2/token";
        String authorizeParams = "code=" + code + 
				"&client_id=" + clientId + 
				"&client_secret=" + clientSecret +
				"&redirect_uri=" + redirectUri + 
				"&grant_type=authorization_code";
        
        String authorizeResponse = executePost(authURL, authorizeParams);
        
        //Parsing out the access token
        JsonParser jp = new JsonParser();
        JsonElement oauth = jp.parse(authorizeResponse);
        JsonObject rootObj = oauth.getAsJsonObject();
        String access_token = rootObj.get("access_token").getAsString();
        
        System.out.println("Got oauth access token: "+access_token);
        
        //Using Temboo Session to get all the calendars with the given account.
        TembooSession session = new TembooSession(accountName, appName, appID);
        GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);
        
        //Setting all the inputs needed to get the calendars
        GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();
        getAllCalendarsInputs.set_ClientSecret(clientSecret);
        getAllCalendarsInputs.set_ClientID(clientId);
        getAllCalendarsInputs.set_AccessToken(access_token);
        
        //Parsing the calendar result list and printing out the calendars to the user.
        GetAllCalendarsResultSet allCalendars = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
        JsonElement root = jp.parse(allCalendars.get_Response());
        rootObj = root.getAsJsonObject();
        JsonArray calendarList = rootObj.get("items").getAsJsonArray();
        System.out.println("============Calendars============");
        for(int i=0; i<calendarList.size(); i++) {
            System.out.println(calendarList.get(i).getAsJsonObject().get("summary").getAsString());
        }
        
        //Selecting the first calendar.
        String calendar = calendarList.get(0).getAsJsonObject().get("id").getAsString();

        //Setting the inputs needed to get all the events for an associated calendar.
        GetAllEvents eventChoreo = new GetAllEvents(session);
        GetAllEventsInputSet getEventInputs = eventChoreo.newInputSet();
        getEventInputs.set_CalendarID(calendar);
        getEventInputs.set_ClientID(clientId);
        getEventInputs.set_ClientSecret(clientSecret);
        getEventInputs.set_AccessToken(access_token);
        
        //Executing the get all events call and parsing through the results.
        GetAllEventsResultSet resultSet = eventChoreo.execute(getEventInputs);
        root = jp.parse(resultSet.get_Response());
        rootObj = root.getAsJsonObject();
        
        //Printing out the results to the user
        JsonArray events = rootObj.get("items").getAsJsonArray();
        System.out.println("=================================");
        System.out.println("Events in "+calendar);
        System.out.println("=================================");
        for(int i=0; i<events.size(); i++){
            String title = events.get(i).getAsJsonObject().get("summary").getAsString();
            String dateTime = events.get(i).getAsJsonObject().get("start").getAsJsonObject().get("dateTime").getAsString();
            System.out.println(title+ " " + dateTime);
        }

        
    }
    
    public static String executePost(String targetURL, String urlParameters) {
		URL url;
		HttpURLConnection connection = null;  
        try {
        	// Create connection
        	url = new URL(targetURL);
        	connection = (HttpURLConnection)url.openConnection();
        	connection.setRequestMethod("POST");
        	connection.setRequestProperty("Content-Type", 
              "application/x-www-form-urlencoded");
            
        	connection.setRequestProperty("Content-Length", "" + 
        			Integer.toString(urlParameters.getBytes().length));
        	connection.setRequestProperty("Content-Language", "en-US");  
            
        	connection.setUseCaches (false);
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
         
        	// Send request
        	DataOutputStream wr = new DataOutputStream (
                     connection.getOutputStream ());
        	wr.writeBytes (urlParameters);
        	wr.flush ();
        	wr.close ();

        	// Read Response   
        	InputStream is = connection.getInputStream();
        	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        	String line;
        	StringBuffer response = new StringBuffer(); 
         
        	while((line = rd.readLine()) != null) {
        		response.append(line);
        		response.append('\r');
        	}
        	rd.close();
         
        	return response.toString();
        } catch (Exception e) {
        	
        	e.printStackTrace();
            return null;
        } finally {
        	if(connection != null) {
        		connection.disconnect(); 
        	}
        }
    }
}
