Tyler Pingree
CS 275 Web & Mobile Application Development
Lab 2
Email: tjp63@drexel.edu tyler.pingree@gmail.com

This lab contains two java files. One for the Calendar viewer using google api's and one
for the calendar viewer using temboo. Each file asks the user to navigate to an authorization
url and get an access key to allow the program to access the users calendars.

The flow of the programs are as follows, since the only difference between the two is how
the information is obtained, the overall flow of the programs are the same.
	The first step is that the access token is obtained from the user when the
user goes to the url provided by the program and allows access. THen the user puts that
code back into the console for the program. Once the code is entered, another post request is
made to get the information needed to get the calendars and events.
	Once all the information is obtained to make requests, the list of calendars is obtained
via temboo or google api. Once the list of calendars is obtained through the api calls, they are
displayed to the user. The first calendar is then selected by default for the next stage of the
program.
	Using the calendarID that was obtianed from the initial api call, all events for that
calendar are obtained through the api call for get all events for an associated calendar.
The eveents for the calendar are then printed out to the user. The title of the event,
and the date and time of the event are obtained from the JSOn response object and it is
then displayed to the user. Once that is complete the program is finished!