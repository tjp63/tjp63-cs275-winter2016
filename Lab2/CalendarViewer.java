/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calendarviewer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 *
 * @author tyler
 */
public class CalendarViewer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Information generated from the google developer console
        String clientId = "584263795773-s375gv3pp39veumnjou3h67ironkj3u8.apps.googleusercontent.com";
        String clientSecret = "GRgAR4ai7sf4gjgAXvAHCCzd";
        String redirectUri = "urn:ietf:wg:oauth:2.0:oob";
        
        //OAuth2.0 URL that will be used.
        String oauthUrl = "https://accounts.google.com/o/oauth2/auth?"
				+ "access_type=offline"
				+ "&client_id=" + clientId
				+ "&scope=https://www.googleapis.com/auth/calendar"
				+ "&response_type=code"
				+ "&redirect_uri=" + redirectUri 
				+ "&state=/profile"
				+ "&approval_prompt=force";
        
        //Getting the code from the user.
        System.out.println("Go to this link and login to provide access to your calendar " + oauthUrl);
        Scanner keyboard = new Scanner(System.in);
        String code = keyboard.nextLine();
        
        //Getting the auth url and setting parameters to do a post command utilizing the
        //executePost method given.
        String authURL = "https://accounts.google.com/o/oauth2/token";
        String authorizeParams = "code=" + code + 
				"&client_id=" + clientId + 
				"&client_secret=" + clientSecret +
				"&redirect_uri=" + redirectUri + 
				"&grant_type=authorization_code";
        String authorizeResponse = executePost(authURL, authorizeParams);
        
        //Parsing the access token from the returned json response from 
        //executing the post command
        JsonParser jp = new JsonParser();
        JsonElement oauth_root = jp.parse(authorizeResponse);
        JsonObject oauth_rootobj = oauth_root.getAsJsonObject();
        String access_token = oauth_rootobj.get("access_token").getAsString();
        System.out.println("Got oauth access token: " + access_token);
        
        String calendarListURL = "https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token="+access_token;
        URL url;
        HttpURLConnection request = null;
        
        //Try/Catch block that will be used to make http calls to the google calendar api
        //This will return all the information about the calendars.
        try{
            //Connection to get all the available calendars for the user's account
            url = new URL(calendarListURL);
            request = (HttpURLConnection) url.openConnection();
            request.connect();        
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootObj = root.getAsJsonObject();
            
            //Getting the list of calendars and printing them out.
            JsonArray calendarList = rootObj.get("items").getAsJsonArray();
            System.out.println("============Calendars============");
            for(int i=0; i<calendarList.size(); i++) {
                System.out.println(calendarList.get(i).getAsJsonObject().get("summary").getAsString());
            }
            
            //The first calendar in the list is selected, and then all of the events within
            //the calendar are pulled out.
            String calendar = calendarList.get(0).getAsJsonObject().get("id").getAsString();
            String eventListURL = "https://www.googleapis.com/calendar/v3/calendars/"+calendar+"/events?access_token="+access_token;
            url = new URL(eventListURL);
            request = (HttpURLConnection) url.openConnection();
            request.connect();
            
            JsonElement calendarRoot = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            rootObj = calendarRoot.getAsJsonObject();
            
            //Taking the array and pulling out the title, date, and time of each event. 
            //All the events are displayed to the user.
            JsonArray events = rootObj.get("items").getAsJsonArray();
            System.out.println("=================================");
            System.out.println("Events in "+calendar);
            System.out.println("=================================");
            for(int i=0; i<events.size(); i++){
                String title = events.get(i).getAsJsonObject().get("summary").getAsString();
                String dateTime = events.get(i).getAsJsonObject().get("start").getAsJsonObject().get("dateTime").getAsString();
                System.out.println(title+ " " + dateTime);
            }
        } catch(Exception e) {
            System.out.println("error setting up connection");
        }
        
    }
    
    /**
     * This is the method that was given to us in example code to execute a HTTP Post
     * command. THis will send the required information needed to get a unique access
     * code for the web application that will be used to access information about
     * the google calendar.
     * @param targetURL
     * @param urlParameters
     * @return 
     */
    public static String executePost(String targetURL, String urlParameters) {
		URL url;
		HttpURLConnection connection = null;  
        try {
        	// Create connection
        	url = new URL(targetURL);
        	connection = (HttpURLConnection)url.openConnection();
        	connection.setRequestMethod("POST");
        	connection.setRequestProperty("Content-Type", 
              "application/x-www-form-urlencoded");
            
        	connection.setRequestProperty("Content-Length", "" + 
        			Integer.toString(urlParameters.getBytes().length));
        	connection.setRequestProperty("Content-Language", "en-US");  
            
        	connection.setUseCaches (false);
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
         
        	// Send request
        	DataOutputStream wr = new DataOutputStream (
                     connection.getOutputStream ());
        	wr.writeBytes (urlParameters);
        	wr.flush ();
        	wr.close ();

        	// Read Response   
        	InputStream is = connection.getInputStream();
        	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        	String line;
        	StringBuffer response = new StringBuffer(); 
         
        	while((line = rd.readLine()) != null) {
        		response.append(line);
        		response.append('\r');
        	}
        	rd.close();
         
        	return response.toString();
        } catch (Exception e) {
        	
        	e.printStackTrace();
            return null;
        } finally {
        	if(connection != null) {
        		connection.disconnect(); 
        	}
        }
    }
    
}
