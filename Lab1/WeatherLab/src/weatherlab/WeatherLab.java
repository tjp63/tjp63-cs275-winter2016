/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weatherlab;

import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author tyler
 */
public class WeatherLab {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception{
        //Initial AP call for location.
        String geoLookUp = "http://api.wunderground.com/api/424a536c5404034e/geolookup/q/autoip.json";
        
        //Setting up and connecting to the URL.
        URL url = new URL(geoLookUp);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();
        
        //Parsing the response and getting the "location"  object from the overall structure.
        //This will be used to get the zip, city, and state fields.
        JsonParser jp = new JsonParser();
        InputStream stream = (InputStream) request.getContent();
        JsonElement root = jp.parse(new InputStreamReader(stream));
        JsonObject rObj = root.getAsJsonObject();
        JsonObject loc = rObj.get("location").getAsJsonObject();
        
        //Getting the string versions of these JSON elements.
        String zip = loc.get("zip").getAsString();
        String city = loc.get("city").getAsString();
        String state = loc.get("state").getAsString();
        
        //Printing out the information in a clean way.
        System.out.println("=================================");
        System.out.println("        Current Location         ");
        System.out.println("City: "+city);
        System.out.println("State: "+state);
        System.out.println("ZipCode: "+zip);
        System.out.println("=================================");
        
        //By using the information obtained from the first API call, we are able to get the hourly weather information.
        //Reused the same code above for the second API code.
        String hLookUp = "http://api.wunderground.com/api/424a536c5404034e/hourly/q/"+state+"/"+city+".json";
        URL urlHr = new URL(hLookUp);
        HttpURLConnection requestHr = (HttpURLConnection) urlHr.openConnection();
        requestHr.connect();
        
        //Getting the response and building a JsonArray of hourly information.
        //The array will be used to print out each different hour.
        InputStream hrStream = (InputStream) requestHr.getContent();
        JsonObject hrRoot = jp.parse(new InputStreamReader(hrStream)).getAsJsonObject();
        JsonArray a = hrRoot.get("hourly_forecast").getAsJsonArray();
        
        //Printing out new section header.
        System.out.println("       Hourly Information        ");
        System.out.println("=================================");
        
        //For loop that will itterate through the JSON Array and place each item into its own JsonElement object.
        //The loop will stop once the end of the array is reached.
        //The loop handles getting the values for date, condition, temp, and humidity.
        //At the end of the loop, each field is formatted and printed out to the user.
        for(JsonElement element : a)
        {
            JsonObject obj = element.getAsJsonObject();
            
            String date = obj.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
            String condition = obj.get("condition").getAsString();
            String temp = obj.get("temp").getAsJsonObject().get("english").getAsString();
            String humidity = obj.get("humidity").getAsString();
            
            System.out.println(String.format("%0$30s", date) + " Condition: " + String.format("%0$-15s",condition) + " Temperature: " + String.format("%0$3s",temp) + "°F" + " Humidity: " + String.format("%0$3s",humidity) + "%");
	    
	}
        
        //Telling the user the program has finished.
        System.out.println("================================");
        System.out.println("Thanks for using the Weather App!");
	
    }
}
