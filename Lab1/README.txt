Tyler Pingree
CS 275 Web & Mobile Application Development
Lab 1
Email: tjp63@drexel.edu tyler.pingree@gmail.com

This lab contains one java file that is used to perform the steps outlined for the
assignment given. There are no command line arguments for this lab, since the key
was hardcoded into the program due to the simplicity of the assignment. To run the 
WeatherApp program, enter the following command

The program flow is as follows, it starts by getting the current location of the computer
running the program via an API call to Underground Weather that utilizes the IP address
of the computer.
	Once that information is obtained, The City, State, and ZipCode, are pulled out of
the response and presented to the user in a nice manner. Then, using the city and state
values that were obtained from the first call, a second call is made to obtain the hourly
weather information for that specific city.
	A for loop is used to go through each hour that is obtained from the new request and prints
out the results for date, temp, condition, humidity, and the temperature. These are all the fields
that were asked to be presented to the user via the instructions.

To test the program, a couple of different things were done. First off, the program was ran as is,
to not only ensure the correct location was being obtained, but that there were no errors with 
the core functionality of the code. Once the assignment seemed complete, the initial API call was
changed to various cities to ensure that the data returned for the "hourly" api call was consistent
for all cities. When it was determined that the "hourly" api call was consistent for every city, the code
was changed back to what was requested by the assignment.