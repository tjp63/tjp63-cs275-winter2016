Tyler Pingree
CS 275 Web & Mobile Application Development
Lab 4
Email: tjp63@drexel.edu tyler.pingree@gmail.com

For this lab, which is the second Android application lab. We were tasked with displaying
the weather to users via stored database calls as well as an Async task that ran in the background
as the app was displaying data. Using the code from the first weather app as well as example
code that handles Async tasks and working with the SQLite database.

Within the on create method in the MainActivity java file, the database is initialized as well
as the Async task that runs in the background. The async task populates the database if there is
nothing currently in there, and then the async task is finished. Once the task finishes,
a custom array adapter is created and within the GetView method of the adapter, the fields are
populated for each item of the ListView. There is another async task that runs in the getView method
that downloads the image from the image url that is given to us via the background async task.

I had some trouble getting the list view to display properly and honestly could not figure
out why the data is not coming into the list view. Everything is populated properly and I even
set break points that are hit when the background task runs so I know there is data coming
into the program.
