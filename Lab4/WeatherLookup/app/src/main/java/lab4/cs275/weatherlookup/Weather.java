package lab4.cs275.weatherlookup;

/**
 * Created by tyler on 2/25/2016.
 */
public class Weather {

    /**
     * Weather object that is used within the CustomListViewAdapter to add information
     * to the screen to be displayed. This will handle all the different fields that
     * we were asked to display.
     *
     */

    private String city;
    private String hour;
    private String date;
    private String humidity;
    private String image_url;

    public Weather(String city, String hour, String date, String humidity, String image_url) {
        this.city = city;
        this.hour = hour;
        this.date = date;
        this.humidity = humidity;
        this.image_url = image_url;
    }

    public Weather(){}

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHour(){
        return hour;
    }

    public void setHour(String hour){
        this.hour = hour;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date){
        this.date = date;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity){
        this.humidity = humidity;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
