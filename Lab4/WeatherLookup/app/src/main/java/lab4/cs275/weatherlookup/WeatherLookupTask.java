package lab4.cs275.weatherlookup;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.w3c.dom.Element;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by tyler on 2/25/2016.
 */
public class WeatherLookupTask extends AsyncTask<Void, Void, Void> {

    ArrayList<Weather> data;
    boolean c = true;
    public ArrayList<Weather> getData() {return data;}
    public Context context;
    public WeatherLookupTask(Context context) {
        data = new ArrayList<Weather>();
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try{
            String geoLookUp = "http://api.wunderground.com/api/424a536c5404034e/geolookup/q/autoip.json";

            //Setting up and connecting to the URL.
            URL url = new URL(geoLookUp);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            //Parsing the response and getting the "location"  object from the overall structure.
            //This will be used to get the zip, city, and state fields.
            JsonParser jp = new JsonParser();
            InputStream stream = (InputStream) request.getContent();
            JsonElement root = jp.parse(new InputStreamReader(stream));
            JsonObject rObj = root.getAsJsonObject();
            JsonObject loc = rObj.get("location").getAsJsonObject();

            String city = loc.get("city").getAsString();
            String state = loc.get("state").getAsString();

            //By using the information obtained from the first API call, we are able to get the hourly weather information.
            //Reused the same code above for the second API code.
            String hLookUp = "http://api.wunderground.com/api/424a536c5404034e/hourly/q/"+state+"/"+city+".json";
            URL urlHr = new URL(hLookUp);
            HttpURLConnection requestHr = (HttpURLConnection) urlHr.openConnection();
            requestHr.connect();

            //Getting the response and building a JsonArray of hourly information.
            //The array will be used to print out each different hour.
            InputStream hrStream = (InputStream) requestHr.getContent();
            JsonObject hrRoot = jp.parse(new InputStreamReader(hrStream)).getAsJsonObject();
            JsonArray a = hrRoot.get("hourly_forecast").getAsJsonArray();

            String hour;
            String date;
            String humidity;
            String image_url;
            for(int i = 0; i < a.size(); i++) {
                hour = a.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("hour").getAsString();
                date = a.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("weekday_name").getAsString();
                humidity = a.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
                image_url = a.get(i).getAsJsonObject().get("icon_url").getAsString();

                Weather weather = new Weather(city, hour, date, humidity, image_url);
                data.add(i, weather);

                Sql_Open_Helper sql_open_helper = new Sql_Open_Helper(this.context);
                SQLiteDatabase db = sql_open_helper.getWritableDatabase();
                if(this.c){
                    sql_open_helper.remake(db);
                    this.c = false;
                }

                ContentValues values = new ContentValues();
                values.put("Temp", humidity);
                values.put("Hour", hour);
                values.put("Date", date);
                values.put("City", city);

                db.insert("Weather_Table", null, values);
                db.close();
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
