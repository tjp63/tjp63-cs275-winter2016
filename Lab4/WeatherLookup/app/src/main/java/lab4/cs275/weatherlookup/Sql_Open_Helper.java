package lab4.cs275.weatherlookup;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by tyler on 2/25/2016.
 */
public class Sql_Open_Helper extends SQLiteOpenHelper {

    //Create table sql call that will be called when the program initially runs.
    String CREATE_WEATHER_TABLE = "CREATE TABLE IF NOT EXISTS "+ "Weather_Table" + "("
            +"Temp"+" TEXT,"+"Hour"+" TEXT,"
            +"Date"+" TEXT,"+"City"+" TEXT"+")";


    //Constructor for the db helper.
    public Sql_Open_Helper(Context context) {
        super(context, "WEATHER-TABLE",null,1);
    }

    //Calls the sql query stored above.
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_WEATHER_TABLE);
    }

    //Updates the table with new information by dropping the previous one.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Weather_table");
        db.execSQL(CREATE_WEATHER_TABLE);
    }

    //Drops the table again if you want the table to be remade.
    public void remake(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS Weather_table");
        db.execSQL(CREATE_WEATHER_TABLE);
    }

    //Does a simple sql query to get the hour of the day to see if
    //there needs to be another database call made.
    public int getHour(SQLiteDatabase db){
        String query = "SELECT * FROM Weather_Table LIMIT 1";
        Cursor cursor = db.rawQuery(query, null);
        int foo = 25;
        if(cursor.moveToFirst()) {
            int hour = Integer.parseInt(cursor.getString(1));
            String date = cursor.getString(2);
            return hour;
        }
        else{
            return foo;
        }
    }

    //This adds a weather object to the database given.
    public ArrayList<Weather> addWeatherToList (SQLiteDatabase db) {
        String small_query = "SELECT * FROM Weather_Table";
        Cursor small_cursor = db.rawQuery(small_query, null);
        ArrayList<Weather> weather_list = new ArrayList<Weather>();
        if(small_cursor.moveToFirst())
        {
            do{
                //Gets the information from the cursor to populate a database entry.
                Weather weather = new Weather();
                weather.setHumidity(small_cursor.getString(0));
                weather.setHour(small_cursor.getString(1));
                weather.setDate(small_cursor.getString(2));
                weather.setCity(small_cursor.getString(3));
                weather_list.add(weather);
                //Does this until there is no more information to add.
            } while(small_cursor.moveToNext());
        }
        return weather_list;
    }
}
