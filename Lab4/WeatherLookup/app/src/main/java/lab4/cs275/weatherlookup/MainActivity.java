package lab4.cs275.weatherlookup;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Creates the sql helper to load the database.
        Sql_Open_Helper sql_open_helper = new Sql_Open_Helper(this);
        SQLiteDatabase db = sql_open_helper.getReadableDatabase();
        int hour = sql_open_helper.getHour(db);

        int current_hour = 14;

        //Creates the listView for the information from the database.
        ArrayList<Weather> weather_list = sql_open_helper.addWeatherToList(db);
        CustomArrayAdapter adapter = new CustomArrayAdapter(this, weather_list, false);
        ListView l = (ListView) findViewById(R.id.listView);
        l.setAdapter(adapter);

        //Creates the listView from the background task to get all the information.
        WeatherLookupTask task = new WeatherLookupTask(this);
        task.execute();
        CustomArrayAdapter adapter2 = new CustomArrayAdapter(this, task.getData(), true);
        ListView l2 = (ListView) findViewById(R.id.listView);
        l2.setAdapter(adapter2);

        //Executes the task to get more data.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
