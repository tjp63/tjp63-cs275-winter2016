package lab4.cs275.weatherlookup;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Created by tyler on 2/28/2016.
 */
public class CustomArrayAdapter extends BaseAdapter {

    //Class Variales needed to access information
    private Context context;
    private ArrayList<Weather> array;
    private boolean b;
    private boolean c;

    //Constructor that will create the CustomArrayAdapter object
    public CustomArrayAdapter(Context context, ArrayList<Weather> objects, boolean b){

        this.context = context;
        this.array = objects;
        this.b = b;
        this.c = true;
    }

    //View Holder that will hold the view for each list item.
    private class ViewHolder {
        ImageView imageView;
        TextView textView;
        TextView textView2;
        TextView textView3;
        TextView textView4;
    }

    //Returns the count of the list view.
    @Override
    public int getCount() {
        return array.size();
    }

    //Gets an item in the list view object.
    @Override
    public Object getItem(int position) {
        return array.get(position);
    }

    //Returns the item id of the object requested.
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        //If convertView equals null, this means that there is no view currently created, so a new
        //one will be made.
        if (convertView == null) {
            //Creating the viewHolder object to store everything.
            holder = new ViewHolder();
            //Getting the listview Object.
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.my_list_item, null);

            //Setting all the values.
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            holder.textView2 = (TextView) convertView.findViewById(R.id.textView2);
            holder.textView3 = (TextView) convertView.findViewById(R.id.textView3);
            holder.textView4 = (TextView) convertView.findViewById(R.id.textView4);

            //Setting the tag for the convertView.
            convertView.setTag(holder);
        } else {
            //View is already created so get it.
            holder = (ViewHolder) convertView.getTag();
        }
        //Sets all the text for the views.
        holder.textView.setText(array.get(position).getCity());
        holder.textView2.setText(array.get(position).getHumidity());
        holder.textView3.setText(array.get(position).getHour());
        holder.textView4.setText(array.get(position).getDate());

        //Downloads the picture if there is one.
        if(b) {
            new DownloadImageTask(holder.imageView).execute(array.get(position).getImage_url());
        }
        return convertView;
    }

    //Class that handles downloading the picture.
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        //Does in background of populating the listView.
        @Override
        protected Bitmap doInBackground(String... params) {
            String urldisplay = params[0];
            Bitmap icon = null;
            //Downloads the image from the url given.
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                icon = BitmapFactory.decodeStream(in);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return icon;
        }

        //Returns the image.
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
