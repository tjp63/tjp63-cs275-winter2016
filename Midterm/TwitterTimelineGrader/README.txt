Tyler Pingree
CS 275 Web & Mobile Application Development
Assignment 1
Email: tjp63@drexel.edu tyler.pingree@gmail.com

For this assignment we were asked to use the Twitter api via Temboo to grab 30 tweets
from a specific user's timeline. Then after the tweets were obtained, we were to use the
wordnik api to determine how many syllables were in each of the words within the tweets.

The program starts by doing initial and final OAuth calls through temboo using the twitter
app key and secret that was obtained through the twitter dev site. Then after the authentication
was complete, the username provided in the program, for starters I used mine, was used to get the
last 30 tweets that were sent from the account. I excluded replies from the tweets that were
brought back since the "@" character caused some problems with the wordnik api.

Each word within a tweet is passed to the wordnik api to determine how many syllables are
within the word. If there are more than two syllables in the word, then teh word is said to be
polysyllabic. All of the polysyllabic words are stored in an array while the program executes, and then
is printed out to the user at the end of the program in the order that they were found.

A grade is then calculated using this formal that was provided from the instructions for the
midterm.
grade = 1.0430 * sqrt(number_of_polysyllables * (30 / number_of_sentences)) + 3.1291 
The number of sentences was set to 30, which is why within my code you will not see that
part of the formula.

Below is the console output for two tests that were ran to confirm program correctness.

Console output for username :tpinggggggg
Please go to: https://api.twitter.com/oauth/authorize?oauth_token=0tSQOQAAAAAAkSztAAABUuCxcOo&oauth_token_secret=pbhTxlw0PqlI5Bk2H3przsbLIWojo7KS&oauth_callback_confirmed=true
Press enter when complete

Polysyllabic word count in 30 tweets: 6
Words: 
1. fraternity
2. pediatric
3. original
4. homophobia
5. stupidity
6. idiotic,
Grade level required to read tweets: 5.683917801722854

Console output for username: JB_Couch
Please go to: https://api.twitter.com/oauth/authorize?oauth_token=WNZFAwAAAAAAkSztAAABUuC_80Q&oauth_token_secret=F7u0Ho3AOFsW1K5FJt8lDrIgzpDQboCb&oauth_callback_confirmed=true
Press enter when complete

Polysyllabic word count in 30 tweets: 5
Words: 
1. casually
2. commentary
3. proportional
4. refrigerator.
5. everything!
Grade level required to read tweets: 5.46131890053228