package twittertimelinegrader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tyler
 */
public class TwitterTimelineGrader {

    
    public static String accessToken; //public static variable to work in various methods
    public static String tokenSecret; //public static variable to work in various methods
    public static String callbackId; //public static variable to work in various methods
    public static String token; //public static variable to work in various methods
    public static String userName = "tpinggggggg"; //My User Name.
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws TembooException, IOException, Exception{
        
        //App key and Secret for Twitter.
        String appKey = "4hqngcyAs2cjoFMCwLWj75NVr";
        String appSecret = "MzF8zH4NF144HSvlbWkRmcClEl45itpU8FE0iC9Eqj1HKPLcPp";
        
        //Wordnik API
        String wordnikAPI = "f5a38cb41a63de2c2a12f0ddf26079b681353adb1b74c8929";
        ArrayList<String> polyWords = new ArrayList<>();
        
        //Creating Temboo session and performing OAuth for the Twitter app.
        TembooSession session = new TembooSession("tylerpingree","myFirstApp","JeIu8x8pJaj8oDmDmIhn27X4Otw7p5zw");
        finalizeOauth(session, appKey, appSecret);
        
        //Creating the UserTimeLine Choreo
        UserTimeline userTimelineChoreo = new UserTimeline(session);
        UserTimelineInputSet userInputs = userTimelineChoreo.newInputSet();
        
        //Setting all the required inputs.
        userInputs.set_ScreenName(userName);
        userInputs.set_AccessToken(accessToken);
        userInputs.set_AccessTokenSecret(tokenSecret);
        userInputs.set_ConsumerKey(appKey);
        userInputs.set_ConsumerSecret(appSecret);
        
        //30 Tweets are needed, Replies are not being included for this program.
        userInputs.set_Count(30);
        userInputs.set_ExcludeReplies(true);
        
        //Executing the choreo and placing the 30 tweets into a JsonArray
        UserTimelineResultSet userResults = userTimelineChoreo.execute(userInputs);
        JsonParser parser = new JsonParser();
        JsonArray userTimeline = (JsonArray) parser.parse(userResults.get_Response());
        
        
        for(int i=0; i<userTimeline.size(); i++) {
            
            //Getting an individual tweet and spliting up the words.
            JsonObject tweets = userTimeline.get(i).getAsJsonObject();
            String txt = tweets.get("text").getAsString();
            String[] words = txt.split(" ");
            
            //For loop that will loop through each word within a given tweet.
            for(int j=0; j<words.length; j++){
                //Removing hashtags, and getting the word ready to be sent in the wordnik URL
                String word = words[j].toLowerCase().replace("#", " ");
                
                //Constructing the URL and connecting to get the wordnik response.
		String sURL = "http://api.wordnik.com:80/v4/word.json/" + word + "/hyphenation?useCanonical=true&limit=50&api_key="+wordnikAPI; 
                URL wordnik = new URL(sURL);
                HttpURLConnection wordRequest = (HttpURLConnection) wordnik.openConnection();
                wordRequest.connect();
                
                try{
                    //Parsing the syllables root into a JSON array.
                    JsonElement syllablesRoot = parser.parse(new InputStreamReader((InputStream) wordRequest.getContent()));
                    JsonArray syllables = syllablesRoot.getAsJsonArray();
                    
                    //Getting the last syllable object.
                    JsonObject last = (syllables.get(syllables.size()-1)).getAsJsonObject();
                    
                    //Checking to see it is greater than 1 syllable
                    int seq = last.get("seq").getAsInt();
                    if(seq > 2) {
                        //If greater, it adds it to the polyWords array list.
                        polyWords.add(word);
                    }
                } catch(Exception e) {
                    
                }
            
            }
        }
        
        //Prints out the Polysyllabic words that were found in order of which they were found.
        int count = 1;
        System.out.println("Polysyllabic word count in 30 tweets: "+polyWords.size());
        System.out.println("Words: ");
        for(String word : polyWords) {
            System.out.println(count+". "+word);
            count++;
        }
        
        //Calculates the reading grade for the tweets and prints it out to the user.
        double grade = 1.0430 * Math.sqrt(polyWords.size())+3.1291;
        System.out.println("Grade level required to read tweets: "+grade);
    }
    
    /**
     *  Method that handles all of the authentication that needs to be done in order to connect to
     *  Both Twitter as well as Temboo. Twitter needs to provide this application with authorization,
     *  and then Temboo also needs authorization. 
     * 
     *  The user will be prompted to navigate to a URL, and then once they click allow, simply 
     *  pressing ENTER in the window will allow the program to continue.
     * 
     * @param session - Temboo session parameters that were created in the main method.
     * @param appKey - appKey which is a main method defined variable.
     * @param appSecret - appSecret with is a main method defined variable.
     * 
     * @throws Exception - Would be thrown if the user does not navigate to the twitter url and allow the application 
     * to continue
     */
    public static void finalizeOauth(TembooSession session, String appKey, String appSecret) throws Exception{
       
        InitializeOAuth authChoreo = new InitializeOAuth(session);
        InitializeOAuth.InitializeOAuthInputSet oAuthInputs = authChoreo.newInputSet();
        oAuthInputs.set_ConsumerKey(appKey);
        oAuthInputs.set_ConsumerSecret(appSecret);
        oAuthInputs.set_AccountName(userName);
        InitializeOAuth.InitializeOAuthResultSet authResults = authChoreo.execute(oAuthInputs);
        
        String authURL = authResults.get_AuthorizationURL();
        
        System.out.println("Please go to: "+authURL);
        System.out.println("Press enter when complete");
        Scanner keyboard = new Scanner(System.in);
        String enter = keyboard.nextLine();
        
        
        callbackId = authResults.get_CallbackID();
        token = authResults.get_OAuthTokenSecret();
        
        FinalizeOAuth finalizeChoreo = new FinalizeOAuth(session);
        FinalizeOAuth.FinalizeOAuthInputSet finalInputs = finalizeChoreo.newInputSet();
        
        finalInputs.set_CallbackID(callbackId);
        finalInputs.set_OAuthTokenSecret(token);
        finalInputs.set_ConsumerKey(appKey);
        finalInputs.set_ConsumerSecret(appSecret);
        finalInputs.set_AccountName(userName);
        
        
        FinalizeOAuth.FinalizeOAuthResultSet finalResults = finalizeChoreo.execute(finalInputs);
        accessToken = finalResults.get_AccessToken();
        tokenSecret = finalResults.get_AccessTokenSecret();
    }
    
}
