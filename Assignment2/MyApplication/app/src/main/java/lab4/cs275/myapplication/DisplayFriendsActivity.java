package lab4.cs275.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cloudmine.api.CMApiCredentials;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.db.LocallySavableCMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;
import com.cloudmine.api.rest.response.LoginResponse;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by tyler on 3/8/2016.
 */
public class DisplayFriendsActivity extends AppCompatActivity {

    public CloudmineUser _user;
    public Context _activityContext;
    public ArrayList<Friend> friends;
    public ArrayList<ListViewObject> _objects = new ArrayList<>();
    public HashMap<String, Friend> userFriends;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_friends);

        //Getting the context from the previous application and initializing credentials again.
        _activityContext = getApplicationContext();
        CMApiCredentials.initialize(getString(R.string.cm_app_id), getString(R.string.cm_api_key), getApplicationContext());
        LoginActivity login = (LoginActivity) getParent();
        Bundle extras = getIntent().getExtras();

        //Getting the User from the previous application.
        _user = (CloudmineUser) extras.getSerializable("user");

        //Setting the name and Email Text Fields for the user.
        String name = _user.getFirstName() + " " +_user.getLastName();
        String email = _user.getEmail();
        TextView nameText = (TextView) findViewById(R.id.nameDisplay);
        nameText.setText(name);
        TextView emailText = (TextView) findViewById(R.id.emailDisplay);
        emailText.setText(email);

        //Getting the cloud mine data.
        /*
        LocallySavableCMObject.loadObjects(this, _user.getFriends(), new Response.Listener<CMObjectResponse>() {
            @Override
            public void onResponse(CMObjectResponse response) {
                for (CMObject object : response.getObjects()) {
                    Friend temp = (Friend) object;
                    userFriends.put(temp.getEmail(), temp);
                }
            }
        });
        */
        //Getting the friends into a list.
        friends = new ArrayList<>();
        friends.add((Friend) extras.getSerializable("friend0"));
        friends.add((Friend) extras.getSerializable("friend1"));
        friends.add((Friend) extras.getSerializable("friend2"));
        friends.add((Friend) extras.getSerializable("friend3"));


        //For loop to go through friends to get data for list View.
        for(Friend friend : friends) {
            String firstName = friend.getFirstName();
            String lastName = friend.getLastName();
            String mail = friend.getEmail();
            ListViewObject tempObj = new ListViewObject(firstName+" "+lastName, mail, friend, _user);
            _objects.add(tempObj);
        }

        //Getting the list view and setting the adapter.
        View listV = findViewById(R.id.listView);
        ListView list = (ListView) listV;
        Context context = list.getContext();
        CustomListViewAdapter adapter = new CustomListViewAdapter(context, _objects);
        list.setAdapter(adapter);

        //onItemLongClickListener that will handle sending an email to the friend.
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ListViewObject temp = _objects.get(position);
                String email = temp.getFriend().getEmail();
                sendEmail(email);
                return false;
            }
        });

        //Setting up the button that will start the google map activity.
        Button b = (Button) findViewById(R.id.showMap);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Code to pass the friends and the user into the google maps activity.
                int count =0;
                Intent mapIntent = new Intent(_activityContext, MapsActivity.class);
                for(Friend friend: friends)
                {
                    mapIntent.putExtra("friend"+count,friend);
                    count++;
                }
                mapIntent.putExtra("user",_user);
                startActivity(mapIntent);
            }
        });


    }


    /***
     * Method that will handle starting the email intent for the user when they long click
     * on an item in the ListView. It will take the email of the person that was clicked and
     * place that email in the To field.
     * @param email
     */
    protected void sendEmail(String email) {

        Log.i("Send email", "");
        String[] TO = new String[] {email};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);


        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "I found you on my Friend Finder App!");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }
        catch (android.content.ActivityNotFoundException ex) {
        }
    }




}
