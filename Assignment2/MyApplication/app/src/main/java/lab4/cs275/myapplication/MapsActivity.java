package lab4.cs275.myapplication;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public ArrayList<Friend> friends = new ArrayList<>();
    public ArrayList<LatLng> locations = new ArrayList<>();
    public CloudmineUser _user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle extras = getIntent().getExtras();
        friends.add((Friend) extras.getSerializable("friend0"));
        friends.add((Friend) extras.getSerializable("friend1"));
        friends.add((Friend) extras.getSerializable("friend2"));
        friends.add((Friend) extras.getSerializable("friend3"));

        for(Friend friend : friends)
        {
            LatLng location = new LatLng(Double.parseDouble(friend.getLatitude()), Double.parseDouble(friend.getLongitude()));
            locations.add(location);
        }

        _user = (CloudmineUser) extras.getSerializable("user");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker").snippet("Snippet"));

        // Enable MyLocation Layer of Google Map
        try{
            // set map type
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            // Get latitude of the current location
            double latitude = Double.parseDouble(_user.getLatitude());

            // Get longitude of the current location
            double longitude = Double.parseDouble(_user.getLongitude());

            // Create a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);

            // Show the current location in Google Map
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            // Zoom in the Google Map
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
            mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("You are here!"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14));


            int friend = 0;
            for(LatLng loc : locations)
            {
                mMap.addMarker(new MarkerOptions().position(loc)).setTitle(friends.get(friend).getFirstName()+" "+friends.get(friend).getLastName());
                friend++;
            }

        } catch(SecurityException e)
        {
            e.printStackTrace();
        }
    }
}