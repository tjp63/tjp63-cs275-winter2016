package lab4.cs275.myapplication;

/**
 * Created by tyler on 2/14/2016.
 */
public class ListViewObject {

    private String field1;
    private String field2;
    private CloudmineUser _user;
    private Friend _friend;

    public ListViewObject(String field1, String field2, Friend friend, CloudmineUser user) {
        this.field1 = field1;
        this.field2 = field2;
        this._friend = friend;
        this._user = user;
    }

    public void setField1(String field)
    {
        field1 = field;
    }

    public String getField1() {
        return field1;
    }

    public void setField2(String field) {
        field2 = field;
    }

    public String getField2(){
        return field2;
    }

    public void setFriend(Friend friend) {_friend = friend;}

    public Friend getFriend() { return _friend; }

    public void setUser(CloudmineUser user) {_user = user;}

    public CloudmineUser getUser() { return _user; }











}
