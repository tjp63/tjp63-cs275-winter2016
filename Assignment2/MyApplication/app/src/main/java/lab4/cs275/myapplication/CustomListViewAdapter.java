package lab4.cs275.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tyler on 2/14/2016.
 */
public class CustomListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private CloudmineUser _user;
    private ArrayList<ListViewObject> objects;
    private ArrayList<String> _listOfFriends;
    private int _position;
    private int mResource;
    private int mDropDownResource;
    private int mFieldId = 0;
    private ListView _lv;
    private String _oldName;
    private String _id;
    private Context mContext;
    private Friend _strip;
    private HashMap<String,Friend> _userStrips;


    private class ViewHolder {
        TextView textView1;
        TextView textView2;
    }

    public CustomListViewAdapter(Context context, ArrayList<ListViewObject> items) {

        inflater = LayoutInflater.from(context);
        mContext = context;
        this.objects =  items;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public ListViewObject getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        _position = position;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.mylist, null);
            holder.textView1 = (TextView) convertView.findViewById(R.id.nameFriend);
            holder.textView2 = (TextView) convertView.findViewById(R.id.emailFriend);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView1.setText(objects.get(position).getField1());
        holder.textView2.setText(objects.get(position).getField2());

        return convertView;


    }










}

