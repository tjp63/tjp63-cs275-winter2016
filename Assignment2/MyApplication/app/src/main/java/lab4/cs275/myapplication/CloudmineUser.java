package lab4.cs275.myapplication;

import com.cloudmine.api.CMUser;

import java.io.Serializable;
import java.util.ArrayList;

public class CloudmineUser extends CMUser implements Serializable{
    public static final String CLASS_NAME = "CloudmineUser";
    public String firstName;
    public String lastName;
    public String email;
    public String latitude;
    public String longitude;
    public ArrayList<String> friends;

    public CloudmineUser(){super();}
    //id is the id of the strip. This id will need to be found before the constructor is called
    //by searching for the strip via stripId.
    public CloudmineUser(String email, String pass, String fn, String ln, String lat, String lon){
        super(email, pass);
        this.firstName=fn;
        this.lastName=ln;
        this.latitude = lat;
        this.longitude = lon;
        this.email = email;
        friends = new ArrayList<String>();
    }

    public CloudmineUser(String email, String pass){
        super(email,pass);
    }



    public String getClassName(){return CLASS_NAME;}
    public String getFirstName() {return firstName;}
    public String getLastName() {return lastName;}
    public String getEmail() {return email;}
    public String getLatitude() {return latitude;}
    public String getLongitude() {return longitude;}
    public ArrayList<String> getFriends() {return friends;}
    public void setFirstName(String fn){this.firstName=fn;}
    public void setLastName(String ln){this.lastName=ln;}
    public void setLatitude(String lat){this.latitude=lat;}
    public void setLongitude(String lon){this.longitude=lon;}
    public void setFriends(ArrayList<String> f) {friends=f;}
    public void addFriend(String s){friends.add(s);}
    public void removeFriend(String s){friends.remove(s);}

}
