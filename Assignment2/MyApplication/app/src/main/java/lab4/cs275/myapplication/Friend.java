package lab4.cs275.myapplication;

import com.cloudmine.api.db.LocallySavableCMObject;

import java.io.Serializable;

/**
 * Created by Greg on 1/30/2016.
 */
public class Friend extends LocallySavableCMObject implements Serializable{
    public static final String CLASS_NAME = "Friend";
    public String firstName;
    public String lastName;
    public String latitude;
    public String longitude;
    public String email;

    public Friend(){
        super();
    }

    public Friend(String f, String ln, String lat, String lon, String em){
        this();
        firstName = f;
        lastName = ln;
        latitude = lat;
        longitude = lon;
        email = em;
    }

    public String getClassName(){
        return CLASS_NAME;
    }

    public String getFirstName() {return firstName;}
    public String getLastName() {return lastName;}
    public String getLatitude() {return latitude;}
    public String getLongitude() {return longitude;}
    public String getEmail() {return email;}

    public void setFirstName (String s) {firstName=s;}
    public void setLastName(String h) {lastName=h;}
    public void setLatitude(String ont) {latitude=ont;}
    public void setLongitude(String oft ) {longitude=oft;}
    public void setEmail(String em) {email = em;}
}

