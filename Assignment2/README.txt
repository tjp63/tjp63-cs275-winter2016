Tyler Pingree
CS 275 Web & Mobile Application Development
Assignment 2
Email: tjp63@drexel.edu tyler.pingree@gmail.com

For this assignment, we were tasked with using Cloudmine to store data about friends locations. Once we
logged into cloudmine, we had to set the current location for the Cloudmine user. Then after the current location was
set we had to list the people who were close to us. Once the list is populated, we had to plot the friends on a map,
as well as give the user the ability to send a friend an email by clicking on them in the list.

There are three activites that run. The activity that logs into the application, the activity that 
handles getting the friends and displaying them in a list view, and then the maps activity which displays the friends
on the map. Each activity starts the next one using the startActivity() method. Once the activity is started, the onCreate
method is called and then the code runs for each activity.

I was unable to implement the code that checks and see when people are moving around and what not
because I was unsure how to manipulate the data since it is all fake. I also had a little trouble with
cloudmine in terms of accessing the data. It was hard to get objects that were already created.